import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Recherche
{	
	private static Node root = null;
	
		public Recherche()
	{
		root = new Node();
	}


	public static void main (String[] args)
	{
		Recherche r = new Recherche();
		int compte = 0;
		System.out.println("Devines 5 nombres dans le fichier");
		for (int i=1; i<6; i++) {
			System.out.print("Entrez le nombre " + i + " que deviner dans le fichier : ");	
			Scanner in = new Scanner(System.in);
			int nombre = in.nextInt();
			boolean trouve = r.recherche(nombre);
			compte += (trouve ? 1 : 0); 
		}
		System.out.println("Trouvage total : " + compte + "nombres");
	}
	
	public static boolean recherche(int nombre)
	{	
			// On loade
			Node current = root;
			int counter = 1;
			long temps = System.currentTimeMillis();
			for (;;) {
				Scanner sc = null;
				try {
					sc = new Scanner(new File("data.txt"));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					System.exit(1);
				}
				String nbS = new String();
				int skip = 0;
				for (;sc.hasNext() && skip < counter; skip++) {
					nbS = sc.next();
				}
				++counter;
				if (nbS == null || !sc.hasNext()) {
					sc.close();
					break;
				}
				Node newNode = new Node();
				Integer scN = new Integer(nbS);
				int value = scN.intValue();
				newNode.setData(value);
				current.setNext(newNode);
				current = current.getNext();
			}
			temps = System.currentTimeMillis() - temps;
			System.out.println("Temps de loadage : " + temps + " ms");
	
			// on fait la recherche
			temps = System.currentTimeMillis();
			counter = 1;
			boolean found = (0 == 1);
			current = root;
			while (true) {
				found = false;
				Scanner sc = null;
				try {
					sc = new Scanner(new File("data.txt"));
				} catch (FileNotFoundException e) {
					// Comme appris dans mon cours de C++
					e.printStackTrace();
					System.exit(1);
				}
				String nbS = new String();
				int skip = 0;
				for (;sc.hasNext() && skip < counter; skip++) {
					nbS = sc.next();
				}
				// pour pas avoir de exception
				if (current != null) {
					current = current.getNext();
				}
				if (nbS == null || current == null) {
					sc.close();
					break;
				}
				Integer nbI = new Integer(nbS);
				int nb = nbI.intValue();
				if (current != null && current.dataIs(nombre)) {
					found = true;
					sc.close();
					break;
				}
				sc.close();
				++counter;
			}
			boolean trouve = found;
			temps = System.currentTimeMillis() - temps;
			System.out.println("Temps de lookuping : " + temps + " ms");
			System.out.println("Nombre " + nombre + " dans le fichier : " + found);
			System.out.println("Ligne du nombre : " + counter);
			return trouve;
	}
}
