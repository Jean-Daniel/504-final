public class Node {
	private int data;
	private Node next;
	
	public Node()
	{
		this.next = null;
		this.data = -1;
	}

	public Node getNext()
	{
		return this.next;
	}
	
	public void setNext(Node next)
	{
		this.next = next;
	}

	public int getData()
	{
		return this.data;
	}
	
	public void setData(int data)
	{
		this.data = data;
	}

	public boolean dataIs(int data)
	{
		return (this.data == data);
	}
}
