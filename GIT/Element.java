public class Element {
	private Point data;
	private Element next;
	
	public Element()
	{
		this.next = null;
		this.data = null;
	}

	public Element getNext()
	{
		return this.next;
	}
	
	public void setNext(Element next)
	{
		this.next = next;
	}

	public Point getData()
	{
		return this.data;
	}
	
	public void setData(Point data)
	{
		this.data = data;
	}

	public boolean dataEquals(Point data)
	{
		return (this.data.equals(data));
	}
}
