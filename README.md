---------------------------------------
420-504 -- EXAMEN FINAL -- Automne 2016
---------------------------------------

PROCÉDURE

- Faites un FORK de ce dépot en le renommant final-504-<votre prénom>.
- Donnez accès à l'usager ngorse.
- Faites un git clone de ce dépot sur votre ordinateur.
- Faites votre examen.
- Une fois votre examen terminé, assurez-vous que vous avez bien fait un commit de tous les fichiers nécessaires.


NOTES
    - Si vous avez une incertitude, faites une supposition et documentez-la.
    - Toute documentation permise.
    - Accès à internet autorisé.
    - Communication interdite.
    - Toutes les réponses doivent être de votre composition. Tout copier-coller venant du web sera considéré comme du plagiat.
   

-------------------------------------------
PARTIE 1 -- TESTS, PERFORMANCE, REFACTORING
-------------------------------------------

Vous travaillez dans une entreprise de jeux. Un stagiaire a programmé un jeu de recherche de nombres dans un fichier. Ce jeu est disponible sous forme de projet Maven dans le répertoire Game/.

Le joueur doit entrer un nombre à chercher 5 fois de suite. Le programme cherche le nombre dans son fichier de données (src/main/resources/data.txt) et compte le nombre de fois où le joueur a donné un nombre qui se trouve dans le fichier.

Votre patron, qui n'y connait rien en programmation, n'est pas très satisfait de la vitesse du jeu et pense que c'est peut-être mal programmé et qu'il y a peut-être aussi des bugs. Il vous demande d'y jeter un oeil et de faire les choses suivantes :


QUESTION A -- AJOUT DE TESTS [15 pts]
-------------------------------------

  Projet : Recherche

  1. Ajoutez des tests pour vous assurer que la méthode recherche(int nombre) fonctionne correctement (10 pts).

     NOTE : Faites un commit à chaque test ajouté.

  2. Faites un mini rapport sur les bugs trouvés et leurs causes mais ne les corrigez pas (5 pts).

     NOTE : À part l'ajout de tests, ne faites aucune modification de code à ce point là !

     NOTE : Faites un commit une fois votre rapport écrit.


QUESTION B -- ANALYSE [20 pts] 
------------------------------

  Projet : Recherche

  Analysez le programme et faites deux mini rapports pour votre tech-lead (qui lui est bon programmeur).

  1. Performance
     a. Faites une liste des problèmes (5 pts)
     b. Proposez des solutions (5 pts)

  2. Qualité
     a. Faites une liste des problèmes (5 pts)
     b. Proposez des solutions (5 pts)

  Note : Faites un commit une fois chaque rapport écrit.


QUESTION C -- AMELIORATIONS [35 pts] 
------------------------------------

  Projet : Recherche

  Faites des commits incrémentaux et documentez vos modifications de manière précise dans les messages de commits (10 pts).

  Modifiez le programme de manière à :
  
  1. Nettoyer le code (5 pts)
  2. Améliorer la performance (10 pts)
  3. Corriger les bugs (5 pts)


-------------------------------------
PARTIE 2 -- MOCK, MODEL-CHECKING, GIT
-------------------------------------


QUESTION D -- Mocking [12 pts]
------------------------------

  Projet : Mocking

  Utilisez Mockito pour tester les fonctions suivantes :

  1. calculeFactoriel pour -3, 5, 12 (6 pts)
  2. le comportement de randomFactoriel quand une exception est reçue (6 pts)
  
  Note : Vous ne devez pas modifier le code qui est dans src/main/java


QUESTION E -- MODEL CHECKING [8 pts]
------------------------------------

  Vous voulez vérifier par model-checking un modèle du dîner de philosophes. Exprimez à l'aide de formules de logique temporelle linéaire (LTL) les propriétés suivantes :

   1. Il n'y a pas de philosophe qui mangera infiniment (4 pts)
   2. un philosophe finit toujours par manger et en attendant il pense (4 pts)


QUESTION F -- GIT [10 pts]
--------------------------

  Projet : GIT

  Ramenez les changements des branches suivantes dans la branche master dans l'ordre indiqué ci-dessous :

  1. dev_alpha [5 pts]
  2. dev_beta [5 pts]
