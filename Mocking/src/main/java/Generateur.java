public class Generateur
{
	int generate() throws NumberFormatException
	{
		double randomDouble = Math.random() * 11;
		if (randomDouble > 10.0) {
			throw new NumberFormatException("too big!");
		}
		int randomInt = (int) randomDouble;
		return randomInt;
	}
}
