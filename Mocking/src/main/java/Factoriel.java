public class Factoriel
{
	private Generateur generateur;
	private int input;
	private int output;
	
	public static void main (String[] args)
	{
		Generateur generateur = new Generateur();
		Factoriel factoriel = new Factoriel(generateur);
		factoriel.randomFactoriel();
		System.out.println("factoriel(" + factoriel.getInput() + ") = " + factoriel.getOutput());
	}
	
	public int getInput()
	{
		return this.input;
	}
	
	public int getOutput()
	{
		return this.output;
	}

	public Factoriel(Generateur generateur)
	{
		this.generateur = generateur;
	}

	public void randomFactoriel()
	{
		try {
			this.input = generateur.generate();
		}
		catch (NumberFormatException e) {
			// defaulting to -1
			this.input = -1;
		}
		output = calculeFactoriel(this.input);
	}

	private int calculeFactoriel(int number)
	{
		if (number < 1) {
			return 1;
		}
		return calculeFactoriel(number - 1) * number;
	}
}
